# README #

### What is this repository for? ###

This site was created as a test project for Xcentium.  

### How do I get set up? ###

This project requires Visual Studio 2017 with .NET Core.
The site does not use a database but it does publish the contents to AWS Elastic Beanstalk.


### Contribution guidelines ###

Feel free to contribute and make this test project better.

### Who do I talk to? ###

Repo Owner: Mark Fulton
Repo Contact: markfulton3@gmail.com