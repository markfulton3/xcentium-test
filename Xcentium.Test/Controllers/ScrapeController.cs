﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using Xcentium.Test.Web.Models;
using HtmlAgilityPack;
using Microsoft.AspNetCore.WebUtilities;


namespace Xcentium.Test.Web.Controllers
{
    public class ScrapeController : Controller
    {
        public IActionResult Index()
        {
            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Index(string PageUrl)
        {

            Scrape pageData = ScrapePageData(PageUrl);
            if (String.IsNullOrWhiteSpace(PageUrl) || String.IsNullOrEmpty(pageData.PageUrl))
            {
                return RedirectToAction("Index", "Home", "Invalid");
            }
            return View(pageData);
        }

        public Scrape ScrapePageData(string url)
        {
            Scrape scrape = new Scrape();

            try
            {
                    if (!url.StartsWith("http"))
                    {
                        url = "http://" + url;
                    }
                    HtmlDocument doc = new HtmlWeb().Load(url);
                    scrape.PageUrl = url;

                    //Get Images
                    scrape.ImageList = GetImages(doc, new Uri(url));

                    //Get Words
                    scrape.WordCount = GetWords(doc);
            }
            catch (Exception e)
            {
                //Allow to go to view.
                scrape.PageUrl = string.Empty;
                scrape.Comment = e.Message;
            }

            return scrape;
        }

        public List<WebImage> GetImages(HtmlDocument doc, Uri baseurl)
        {
            List<WebImage> _images = new List<WebImage>();

            IEnumerable<String> urls = doc.DocumentNode.Descendants("img")
                                       .Select(e => e.GetAttributeValue("src", null))
                                       .Where(s => !String.IsNullOrEmpty(s));

            foreach (String url in urls)
            {
                var _url = new Uri(baseurl, url);
                _images.Add(new WebImage { ImageContext = "img", ImageUrl = _url.AbsoluteUri });
            }

            return _images;
        }

        public List<WebWord> GetWords(HtmlDocument doc)
        {
            List<WebWord> _words = new List<WebWord>();
            string wordstring = string.Empty;
            IEnumerable<HtmlNode> nodes = doc.DocumentNode.Descendants()
                                             .Where(n => n.NodeType == HtmlNodeType.Text
                                                      && n.ParentNode.Name != "script"
                                                      && n.ParentNode.Name != "style");

            foreach (HtmlNode node in nodes)
            {
                string _w = System.Net.WebUtility.HtmlDecode(node.InnerText).Replace("-","").Trim();

                if (!string.IsNullOrWhiteSpace(_w))
                {
                    wordstring += _w;
                }

            }

            var ws = wordstring.Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries)
                                        .GroupBy(r => r)
                                        .Select(grp => new WebWord
                                        {
                                            Word = grp.Key,
                                            Count = grp.Count()
                                        })
                                        .OrderByDescending(x => x.Count);

            foreach (WebWord w in ws)
            {
                _words.Add(new WebWord { Word = w.Word, Count = w.Count });
            }

            return _words;
        }

    }
}