﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Xcentium.Test.Web.Models
{

    public class WebImage
    {
        public string ImageUrl { get; set; }
        public string ImageContext { get; set; }
        public int Width { get; set; }
        public int Length { get; set; }
        public int Size { get; set; }
    }

    public class WebWord
    {
        public string Word { get; set; }
        public int Count { get; set; }
    }

    public class Scrape
    {
        public string PageUrl { get; set; }
        public string Comment { get; set; }
        public List<WebImage> ImageList { get; set; }
        public List<WebWord> WordCount { get; set; }

    }
}
