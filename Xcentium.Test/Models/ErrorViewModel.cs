using System;
using Microsoft.AspNetCore.Mvc;

namespace Xcentium.Test.Web.Models
{
    public class ErrorViewModel
    {
        public string RequestId { get; set; }

        public bool ShowRequestId => !string.IsNullOrEmpty(RequestId);
    }
}